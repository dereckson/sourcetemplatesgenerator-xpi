// The main module of the SourceTemplatesGenerator Add-on.

var Widget = require("widget").Widget;
var tabs = require('tabs');

exports.main = function() {
    new Widget({
        id: "sourcetemplatesgenerator-widget-main",
        
        label: "Source Templates Generator",
        
        contentURL: "http://assets.dereckson.be/Mozilla/Addons/SourceTemplatesGenerator/reference.png",

        onClick: function(event) {
            var gateUrl = "http://www.dereckson.be/tools/SourceTemplatesGenerator/?URL=";
            tabs.open(gateUrl + encodeURIComponent(tabs.activeTab.url));
        }
    });
};
